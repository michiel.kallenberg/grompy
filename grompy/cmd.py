# -*- coding: utf-8 -*-
# Copyright (c) 2021 Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), April 2021
from pathlib import Path
import sys
import click

from .load_data import load_data
from .check_files import check_grompy_inputs
from .init import init
from . import __version__

@click.group()
def cli():
    pass


@click.command("init")
@click.argument("year", type=click.INT)
@click.argument("input_path", type=click.Path(exists=True))
def init_grompy(year, input_path):
    input_path = Path(input_path)
    year = int(year)
    if year not in list(range(2000,2030)):
        print("YEAR must be in range 2000..2030")
        sys.exit()
    init(year, input_path)


@click.command("check")
@click.argument("grompy_yaml", type=click.Path(exists=True))
def check_grompy(grompy_yaml):
    grompy_yaml = Path(grompy_yaml)
    check_grompy_inputs(grompy_yaml)


@click.command("load")
@click.argument("grompy_yaml", type=click.Path(exists=True))
@click.option("--parcel_info_only", is_flag=True)
@click.option("--parallel", is_flag=True)
def load_grompy(grompy_yaml, parcel_info_only, parallel):
    grompy_yaml = Path(grompy_yaml)
    load_data(grompy_yaml, parcel_info_only, parallel)


@click.group()
@click.version_option(__version__)
@click.pass_context
def cli(ctx):
    pass


cli.add_command(init_grompy)
cli.add_command(check_grompy)
cli.add_command(load_grompy)


def main():
    cli()


if __name__ == "__main__":
    cli()
