# -*- coding: utf-8 -*-
# Copyright (c) 2021 Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), April 2021
import multiprocessing as mp
import traceback
from pathlib import Path
from packaging import version

import yaml
import sqlalchemy.exc
from sqlalchemy import MetaData, Table, Column, Integer, Date, Float, Text, create_engine
# from sqlalchemy.event import listen
# from sqlalchemy.pool import Pool
from pyproj import Transformer
from . import __version__

def take_first(iter):
    for i in iter:
        return i


def count_lines(files):
    """Checks the number of lines in the input CSV files.

    They should all be the same else return None.
    """
    counts = {}
    print("Checking number of lines:")
    for fname in files:
        with open(fname) as my_file:
            c = sum(1 for _ in my_file)
        counts[fname] = c
        print(f" - {fname}: {c}")

    if len(set(counts.values())) > 1:
        msg = "WARNING: CSV files do not have the same number of " \
              "rows! Grompy will manage but it may indicate a problem in " \
              "the data."
        print(msg)
    else:
        print("OK: CSV files all have same length")

    return take_first(counts.values())


def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = printEnd, flush=True)
    # Print New Line on Complete
    if iteration == total:
        print()


def prepare_db(dsn, table_name, bands):
    def on_connect(dbapi_connection, connection_record):
        # disable pysqlite's emitting of the BEGIN statement entirely.
        # also stops it from emitting COMMIT before any DDL
        # see: https://docs.sqlalchemy.org/en/13/dialects/sqlite.html#serializable-isolation-savepoints-transactional-ddl
        #      https://docs.sqlalchemy.org/en/13/core/event.html
        dbapi_connection.isolation_level = None

    def on_begin(conn):
        # emit our own BEGIN
        conn.execute("BEGIN EXCLUSIVE")

    engine = create_engine(dsn)
    # allow locking of database for concurrency
    # listen(engine, "connect", on_connect)
    # listen(engine, "begin", on_begin)

    meta = MetaData(engine)
    tbl = Table(table_name, meta,
                Column('fieldID', Integer, primary_key=True, nullable=False),
                Column('day', Date, primary_key=True, nullable=False),
                )
    for col_name in bands:
        tbl.append_column(Column(col_name, Float, nullable=True))

    try:
        tbl.drop()
    except sqlalchemy.exc.OperationalError:
        pass
    tbl.create()

    return engine


class Process(mp.Process):
    def __init__(self, *args, **kwargs):
        mp.Process.__init__(self, *args, **kwargs)
        self._parent_conn, self._child_conn = mp.Pipe()
        self._exception = None

    def run(self):
        try:
            mp.Process.run(self)
            self._child_conn.send(None)
        except Exception as e:
            tb = traceback.format_exc()
            self._child_conn.send((e, tb))
            # raise e  # You can still rise this exception if you need to

    @property
    def exception(self):
        if self._parent_conn.poll():
            self._exception = self._parent_conn.recv()
        return self._exception


def make_path_absolute(grompy_yaml, filepath):
    """Makes a path absolute, if a relative path is found it is asssumed to be relative
    to the location of grompy_conf

    :param grompy_yaml: the Path() of gromp_conf
    :param filepath: the (possible relative) Path of the input file

    :return: the absolute path to the file
    """
    if not filepath.is_absolute():
        filepath = grompy_yaml.parent / filepath

    return filepath


def open_DB_connection(grompy_yaml, dsn):
    """This converts a DSN with a relative paths to an sqlite database to an absolute path.

    Other database DSNs are left untouched.
    """

    if dsn.startswith("sqlite"):
        sqlite_path = dsn.replace("sqlite:///", "")
        sqlite_path = make_path_absolute(grompy_yaml, Path(sqlite_path))
        dsn = f"sqlite:///{sqlite_path}"
    engine = create_engine(dsn)
    engine.connect()
    return engine


def get_latlon_from_RD(x, y, _cache={}):
    """Converts given X,Y in the Dutch RD coordinate system towards latitude longitude in WGS84

    For checking:
    in RD coordinate system X, Y: 197903, 309096
    should be in WGS84 latitude, longitude: 50.770195900951045, 5.995343676754713

    """
    rd_stelsel = "EPSG:7415"
    latlon_wgs84 = "EPSG:4326"
    try:
        rd2wgs84 = _cache["rd2wgs84"]
    except KeyError:
        rd2wgs84 = _cache["rd2wgs84"] = Transformer.from_crs(rd_stelsel, latlon_wgs84)

    return rd2wgs84.transform(x, y)


def check_grompy_version(grompy_yaml):
    grompy_conf = yaml.safe_load(open(grompy_yaml))
    v1 = version.parse(grompy_conf["grompy"]["version"])
    v2 = version.parse(__version__)
    if (v1.major, v1.minor) != (v1.major, v1.minor):
        msg = f"grompy.yaml ({v1}) does not match grompy version ({v2})! " \
              "Generate a new grompy.yaml file with `grompy init` " \
              "and adapt it."
        raise RuntimeError(msg)