# -*- coding: utf-8 -*-
# Copyright (c) 2021 Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), April 2021
from pathlib import Path
import click


def init(year, input_path=None):
    """Initializes a fresh 'grompy.yaml' at the input_path location or the current
    working directory otherwise.
    """

    yaml_template_fname = Path(__file__).parent / "grompy.yaml.template"
    with open(yaml_template_fname, "rb") as fp:
        yaml_conf = fp.read().decode()

    if input_path is None:
        input_path = Path(".")
    else:
        input_path = Path(input_path)
    # input_path = input_path.absolute()
    yaml_conf = yaml_conf.format(input_path=input_path, year=year)

    yaml_output_fname = Path.cwd() / "grompy.yaml"
    if yaml_output_fname.exists():
        click.confirm('A grompy.yaml file exists. Overwrite it?', abort=True)
    with open("grompy.yaml", "w") as fp:
        fp.write(yaml_conf)
