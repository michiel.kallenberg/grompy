# -*- coding: utf-8 -*-
# Copyright (c) 2021 Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), April 2021
import sys
import uuid
from pathlib import Path
import time
import sqlite3
import multiprocessing as mp
import zlib
import pickle
import csv
import traceback as tb

import sqlalchemy as sa
import sqlalchemy.exc
import pandas as pd
import geopandas as gpd
import numpy as np
import yaml
from sqlitedict import SqliteDict

from .util import prepare_db, printProgressBar, Process, make_path_absolute, open_DB_connection, get_latlon_from_RD

dummy_date = "19000101"


def my_encode(obj):
    return sqlite3.Binary(zlib.compress(pickle.dumps(obj, pickle.HIGHEST_PROTOCOL)))

def my_decode(obj):
    return pickle.loads(zlib.decompress(bytes(obj)))


def convert_csv_to_store(fname_csv):
    """Converts the CSV file into an SQLite key-value store with filename fname_csv + ".db"
    """

    fname_csv_db = fname_csv.with_suffix(f"{fname_csv.suffix}.db")
    fname_csv_tmp = fname_csv.with_suffix(f".{uuid.uuid4()}.tmp")

    if not fname_csv_db.exists():
        with SqliteDict(fname_csv_tmp, encode=my_encode, decode=my_decode, autocommit=True) as db:
            with open(fname_csv, newline="") as fp_csv:
                reader = csv.reader(fp_csv)
                fieldnames = next(reader)
                db["header"] = fieldnames[1:]
                for row in reader:
                    fid = int(row.pop(0))
                    db[fid] = row

        fname_csv_tmp.rename(fname_csv_db)


def process_csv_stores(grompy_yaml, datasets, parallel=False):

    csv_fnames = []
    for dataset_name, description in datasets.items():
        if description is None:
            continue
        for csv_fname in description["bands"].values():
            csv_fname = make_path_absolute(grompy_yaml, Path(csv_fname))
            csv_fnames.append(csv_fname)

    print(f"Creating stores for {len(csv_fnames)} CSV files... ", end="", flush=True)
    if parallel:
        n = min(mp.cpu_count(), len(csv_fnames))
        with mp.Pool(n) as pool:
            pool.map(convert_csv_to_store, csv_fnames)
    else:
        for csv_fname in csv_fnames:
            convert_csv_to_store(csv_fname)


class CSVLoadingError(Exception):
    """Exception for raising errors related to CSV reading
    """

    def __init__(self, error, trace_back):
        self.error = error
        self.traceback = trace_back
        super().__init__()


def load_parcel_info(grompy_yaml, gpkg_fname, counts_file_10m, counts_file_20m, counts_file_25m, shape_file, layer_name):
    """Loads the parcel info from the shapefile into a consistent geopackage

    :param gpkg_fname: File name where to write results as geopackage
    :param counts_file_10m: CSV file from which pixel counts should be read for the 10m bands
    :param counts_file_20m: CSV file from which pixel counts should be read for the 20m bands
    :param counts_file_25m: CSV file from which pixel counts should be read for the 25m bands
    :param shape_file: shapefile which should be read for parcel info.
    :param layer_name: name of the geopackage layer to write parcel info into

    :return:
    """
    shp_fname = make_path_absolute(grompy_yaml, Path(shape_file))
    df = gpd.read_file(shp_fname)
    df["fieldid"] = df.fieldid.astype(np.int32)
    df = df.set_index("fieldid")

    # Compute latitude/longitude of field centroids
    r = []
    for row in df.itertuples():
        x, y = float(row.geometry.centroid.x), float(row.geometry.centroid.y)
        lon, lat = get_latlon_from_RD(x, y)
        r.append({"fieldid": row.Index, "latitude": lat, "longitude":lon, "rdx": x, "rdy": y})
    df_latlon = pd.DataFrame(r).set_index("fieldid")
    df = df.join(df_latlon)

    # add pixel counts
    for fname, col_name in [(counts_file_10m, "counts_10m"),
                            (counts_file_20m, "counts_20m"),
                            (counts_file_25m, "counts_25m")]:
        fname_counts = make_path_absolute(grompy_yaml, Path(fname))
        df_counts = pd.read_csv(fname_counts)
        df_counts.set_index("field_ID", inplace=True)
        df[col_name] = df_counts["count"]
        # Check for null values, e.g. fields without a count
        ix = df[col_name].isnull()
        if any(ix):
            df.loc[ix, col_name] = 0

    df_out = gpd.GeoDataFrame({"fieldID": df.index,
                               "year": df.year.astype(np.int32),
                               "counts_10m": df.counts_10m.astype(np.int32),
                               "counts_20m": df.counts_20m.astype(np.int32),
                               "counts_25m": df.counts_25m.astype(np.int32),
                               "area_ha": df.geometry.area / 1e4,
                               "rdx": df.rdx,
                               "rdy": df.rdy,
                               "longitude": df.longitude,
                               "latitude": df.latitude,
                               "cat_gewasc": df.cat_gewasc.apply(str),
                               "gws_gewasc": df.gws_gewasc.astype(np.int32),
                               "provincie": df.provincie.apply(str),
                               "gemeente": df.gemeente.apply(str),
                               "regio": df.regio.apply(str),
                               "pc4": df.PC4.apply(str),
                               "AHN2": df.AHN2,
                               "woonplaats": df.woonplaats.apply(str),
                               "waterschap": df.waterschap.apply(str),
                               "S2tiles": df.S2Tiles.apply(str),
                               "geometry": df.geometry
                              }, crs=df.crs)
    df_out.reset_index(drop=True, inplace=True)

    df_out.to_file(gpkg_fname, layer=layer_name, driver="GPKG")
    field_ids = set(df_out.fieldID)
    df_out = df = df_counts = None

    return field_ids


def process_rows(rows, fieldID):
    """Process a single row from different CSV files and convert them
    into a DataFrame.

    :param rows: a dict with column names as keys and a CSV row as values.

    returns a pandas DataFrame
    """
    df = pd.DataFrame()
    for column_name, row in rows.items():
        if "count" in row:
            count = row.pop("count")
        recs = []
        for sdate, value in row.items():
            value = float(value)
            if value == 0.:
                continue
            recs.append({"day": sdate, "value": float(value), "band": column_name})
        if not recs:  # only zero (null) values for the column
            # We add one dummy record to make sure we can create the dataframe properly
            recs.append({"day": dummy_date, "value": None, "band": column_name})
        df_tmp = pd.DataFrame(recs)
        df_tmp["day"] = pd.to_datetime(df_tmp.day).dt.date
        df = pd.concat([df, df_tmp])
    df = df.pivot(index="day", columns="band", values="value")

    # Filter out the dummy records
    ix = (df.index == pd.to_datetime(dummy_date).date())
    if any(ix):
        df = df[~ix]
    # Turn index back into a column
    df.reset_index(inplace=True)

    # Add FieldID to the dataframe
    df["fieldID"] = int(fieldID)

    return df


def write_to_database(engine, dataset_name, csv_readers, field_ids, child_conn=None, total_lines=None):
    """routine writes data from a set of CSV files into the database

    :param engine: the database engine to be used
    :param dataset_name: the name of the dataset, will be used as output table name
    :param csv_readers: the set of CSV DictReaders (one per CSV file)
    :param child_conn: The pipe to report progress in loading data from file
                       if None it is assumed that loading is serial not parallel
    :return:
    """
    if total_lines is not None:
        printProgressBar(0, total_lines, decimals=2, length=50)

    meta = sa.MetaData(engine)
    tbl = sa.Table(dataset_name, meta, autoload=True)
    ins = tbl.insert()

    headers = {column_name: reader["header"] for column_name, reader in csv_readers.items()}
    for i, fid in enumerate(field_ids):
        rows = {}
        for column_name, reader in csv_readers.items():
            try:
                band_values = reader[fid]
            except KeyError as e:
                band_values = [0.] * len(headers)
            header = headers[column_name]
            row = {hdr: value for hdr, value in zip(header, band_values)}
            rows[column_name] = row

        df = process_rows(rows, fid)
        if len(df) == 0:
            continue

        try:
            recs = df.to_dict(orient="records")
            with engine.begin() as DBconn:
                DBconn.execute(ins, recs)
                # df.to_sql(dataset_name, DBconn, if_exists="append", index=False, method="multi")
        except sa.exc.IntegrityError as e:
            print(f"Field ID {df.fieldID.unique()} failed to insert in table {dataset_name}")
        if i % 100 == 0:
            if child_conn is not None:
                child_conn.send({dataset_name: i})
            else:
                printProgressBar(i, total_lines, decimals=2, length=50)

    # Make sure the progressbar goes up to 100% when done
    if total_lines is not None:
        printProgressBar(total_lines, total_lines, decimals=2, length=50)


def write_sensor_info(engine, sensor_info):
    """Write info about sensors in to the database provided by engine

    @param engine: the database engine
    @param sensor_info: the path to the CSV file containing sensor info

    The CSV files should look like::

        Date,Sensor,Overpass
        20210706,S2B,centraal
        20210709,S2B,west
        20210711,S2A,centraal
        20210717,S2A,zeeland
        20210721,S2A,centraal
        20210722,S2B,zeeland
        20210726,S2B,centraal

    and contain a record for all days within the grompy database.
    """
    df = pd.read_csv(sensor_info)
    df = df.rename(columns=lambda x: x.lower())
    df["date"] = pd.to_datetime(df.date, format="%Y%m%d").dt.date
    df.to_sql("sensor_info", engine, index=False, if_exists="replace",
              dtype={"date": sa.types.Date(),
                     "sensor": sa.types.Text(),
                     "overpass": sa.types.Text()}
              )


def load_satellite_csv(grompy_yaml, child_conn, dataset_name, field_ids, dsn, bands, sensor_info, **kwargs):
    """Main routine for starting a parallel loader process.
    """
    
    csv_readers = {}
    for column_name, csv_fname in bands.items():
        csv_fpath = make_path_absolute(grompy_yaml, Path(csv_fname))
        csv_db_fpath = csv_fpath.with_suffix(csv_fpath.suffix + ".db")
        csv_readers[column_name] = SqliteDict(csv_db_fpath, encode=my_encode, decode=my_decode)
    engine = prepare_db(dsn, table_name=dataset_name, bands=csv_readers.keys())
    write_to_database(engine, dataset_name, csv_readers, field_ids, child_conn)
    write_sensor_info(engine, sensor_info)


def start_parallel_loading(grompy_yaml, datasets, field_ids):
    """Start loading CSV files in parallel

    :param grompy_yaml: the full path to the grompy.yaml file
    :param datasets: the datasets to load
    :param field_ids: the set of unique field IDS to process

    :return: a tuple with three elements:
             - a list with references to the running processes
             - the parent connection of the pipe to which the proceses communicate progress
             - the number of lines per dataset being processes
    """
    process_list = []
    parent_conn, child_conn = mp.Pipe()
    for dataset_name, description in datasets.items():
        if description is None:
            continue

        print(f"Starting loading of: {dataset_name}")
        p = Process(target=load_satellite_csv, args=(grompy_yaml, child_conn, dataset_name,field_ids), kwargs=description)
        process_list.append(p)
    for p in process_list:
        p.start()

    return process_list, parent_conn


def start_serial_loading(grompy_yaml, datasets, field_ids):
    """Start loading CSV files in sequence
    :param datasets:
    :return:
    """

    for dataset_name, description in datasets.items():
        if description is None:
            continue

        print(f"Starting loading of: {dataset_name}")
        csv_readers = {}
        for column_name, csv_fname in description["bands"].items():
            csv_fpath = make_path_absolute(grompy_yaml, Path(csv_fname))
            csv_db_fpath = csv_fpath.with_suffix(csv_fpath.suffix + ".db")
            csv_readers[column_name] = SqliteDict(csv_db_fpath, encode=my_encode, decode=my_decode)
        engine = prepare_db(description["dsn"], table_name=dataset_name, bands=csv_readers.keys())
        write_to_database(engine, dataset_name, csv_readers, field_ids, total_lines=len(field_ids))
        write_sensor_info(engine, description["sensor_info"])


def monitor_parallel_loading(process_list, parent_conn, field_ids):
    """Monitors the execution of parallel loading and updates the
    progressbar.
    """
    total_lines = len(process_list) * len(field_ids)
    lines_processed = 0
    printProgressBar(lines_processed, total_lines, decimals=2, length=50)
    exit = False
    try:
        processes = [p for p in process_list if p.is_alive()]
        while processes:
            for p in process_list:
                if parent_conn.poll():
                    lines_processed = 0
                    mesg = parent_conn.recv()
                    if mesg:
                        for dataset_name, nlines in mesg.items():
                            lines_processed += nlines
                    else:  # we got a zero length message
                        print("Zero length message received from parent_conn.recv()."
                              "Loading may continue, but progressbar is probably incorrect.")
                if p.exception:
                    error, traceback = p.exception
                    raise CSVLoadingError(error, traceback)
            printProgressBar(lines_processed, total_lines, decimals=2, length=50)
            time.sleep(3)
            processes = [p for p in process_list if p.is_alive()]

    except KeyboardInterrupt:
        exit = True
        print("Terminated on user request!")
    except CSVLoadingError as e:
        exit = True
        print("Loading failed in one or more files:")
        print(e.error)
        print(e.traceback)
    except Exception as e:
        exit = True
        print("Unknown error caused loading failed in one or more files:")
        tb.print_exc()
    finally:
        for p in process_list:
            p.kill()
        if exit:
            sys.exit()

    # Loading done, force bar to 100%
    printProgressBar(total_lines, total_lines, decimals=2, length=50)


def load_data(grompy_yaml, parcel_info_only, parallel=False):
    """Loads data pointed to by the YAML config file.

    :param grompy_yaml: the path to grompy configuration file
    :param parcel_info_only: Only load parcel info, but no satellite data
    :param parallel: Load data using multiple CPUs, default False
    """

    grompy_conf = yaml.safe_load(open(grompy_yaml))

    # process CSV files to SQLite stores first
    process_csv_stores(grompy_yaml, grompy_conf["datasets"], parallel)
    print("done!")

    # First load parcel info
    print("Start loading parcel information. This will take some time... ", end="", flush=True)
    parcel_info = grompy_conf.pop("parcel_info")
    field_ids = load_parcel_info(grompy_yaml, **parcel_info)
    print("done!")

    if not parcel_info_only:
        if parallel:
            process_list, parent_conn = start_parallel_loading(grompy_yaml, grompy_conf["datasets"], field_ids)
            monitor_parallel_loading(process_list, parent_conn, field_ids)
        else:
            start_serial_loading(grompy_yaml, grompy_conf["datasets"], field_ids)
        print("Loading finished successfully.")