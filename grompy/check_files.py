# -*- coding: utf-8 -*-
# Copyright (c) 2021 Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), April 2021
from pathlib import Path
import sys

import geopandas as gpd
import sqlalchemy as sa
import sqlalchemy.exc
import yaml

from .util import count_lines, make_path_absolute, check_grompy_version

ALL_CHECKS_OK = True


def check_DB_connection(grompy_yaml, dsn):
    global ALL_CHECKS_OK
    if dsn.startswith("sqlite"):
        sqlite_path = dsn.replace("sqlite:///", "")
        sqlite_path = make_path_absolute(grompy_yaml, Path(sqlite_path))
        dsn = f"sqlite:///{sqlite_path}"
    try:
        e = sa.create_engine(dsn)
        e.connect()
        print(f"OK: Connection seems fine for: {dsn}")
        return True
    except sa.exc.SQLAlchemyError as e:
        print(f"ERROR: Failed making DB connection: {dsn}")
        ALL_CHECKS_OK = False
        return False


def check_parcel_info(grompy_yaml, counts_file_10m, counts_file_20m, counts_file_25m, shape_file, **kwargs):
    global ALL_CHECKS_OK
    # Check if files exist
    for fname, col_name in [(counts_file_10m, "counts_10m"),
                            (counts_file_20m, "counts_20m"),
                            (counts_file_25m, "counts_25m")]:
        fname_pixcounts = make_path_absolute(grompy_yaml, Path(fname))
        if fname_pixcounts.exists():
            print(f"OK: pixel {col_name} file exists!")
        else:
            print(f"ERROR: Missing file: {fname_pixcounts}")
            ALL_CHECKS_OK = False

    fname_shape_file = make_path_absolute(grompy_yaml, Path(shape_file))
    if fname_shape_file.exists():
        print(f"OK: shape file with parcel info exists!")
    else:
        print(f"ERROR: Missing file: {fname_shape_file}")
        ALL_CHECKS_OK = False

    if fname_shape_file.exists():
        gdf = gpd.read_file(fname_shape_file, rows=1)
        for column in ["fieldid", "year", "cat_gewasc", "gws_gewasc", "gws_gewas", "provincie",
                       "gemeente", "regio", "PC4", "woonplaats", "waterschap", "S2Tiles", "AHN2"]:
            if column not in gdf.columns:
                print(f"ERROR: Attribute {column} missing in BRP shapefile")
                ALL_CHECKS_OK = False

def check_CSV_inputs(grompy_yaml, dsn, bands, **kwargs):
    global ALL_CHECKS_OK

    n_tested = 0
    fnames = []
    for key, fname in bands.items():
        fpath = make_path_absolute(grompy_yaml, Path(fname))
        fnames.append(fpath)
        if fpath.exists():
            n_tested += 1
        else:
            print(f"ERROR: Cannot find CSV file: {fpath}")
            ALL_CHECKS_OK = False

    nlines = None
    if n_tested == len(bands):
        nlines = count_lines(fnames)
        if nlines is None:
            ALL_CHECKS_OK = False

    db_OK = check_DB_connection(grompy_yaml, dsn)

    return nlines, db_OK


def check_grompy_inputs(grompy_yaml):
    # First check version number in grompy.yaml
    try:
        check_grompy_version(grompy_yaml)
    except RuntimeError as e:
        print(e)
        sys.exit()

    grompy_conf = yaml.safe_load(open(grompy_yaml))
    parcel_info = grompy_conf.pop("parcel_info")
    check_parcel_info(grompy_yaml, **parcel_info)
    for dataset_name, description in grompy_conf["datasets"].items():
        if description is None:  # Dataset has not been defined
            print(f"Skipping {dataset_name}, no inputs defined.")
            continue
        n, OK = check_CSV_inputs(grompy_yaml, **description)
        description.update({"nlines": n, "DBcheck": OK})

    if ALL_CHECKS_OK:
        print("OK! All inputs seem fine.")
        grompy_conf["parcel_info"] = parcel_info
        yaml.safe_dump(grompy_conf, open(grompy_yaml, "w"))
    else:
        print("ERRORS FOUND! Check log messages.")
