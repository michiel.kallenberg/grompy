CREATE TABLE perceels_info (
	fieldID BIGINT,
	year INTEGER, 
	pixcount INTEGER, 
	area_ha FLOAT, 
	cat_gewasc TEXT, 
	gws_gewasc INTEGER, 
	gws_gewas TEXT, 
	provincie TEXT, 
	gemeente TEXT, 
	regio TEXT, 
	pc4 TEXT, 
	woonplaats TEXT, 
	waterschap TEXT,
    primary key (fieldID)
);

create index ix_pixcount on perceels_info(pixcount);
create index ix_gewas_area  on perceels_info(gws_gewasc, area_ha);
create index ix_gewas_pixcount on perceels_info(gws_gewasc, pixcount);
create index ix_gewas_provincie on perceels_info(gws_gewasc, provincie);
